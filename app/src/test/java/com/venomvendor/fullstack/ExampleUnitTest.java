package com.venomvendor.fullstack;

import com.venomvendor.fullstack.util.MathUtil;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {

    @Test
    public void clampIntTest() {
        assertEquals(4, MathUtil.clamp(2, 4, 6));
        assertEquals(4, MathUtil.clamp(4, 2, 6));
        assertEquals(2, MathUtil.clamp(6, 4, 2));
    }

    @Test
    public void clampDoubleTest() {
        assertEquals(4, MathUtil.clamp(2D, 4D, 6D), 0);
    }

    @Test
    public void clampFloatTest() {
        assertEquals(4, MathUtil.clamp(2F, 4F, 6F), 0);
    }
}

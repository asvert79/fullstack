package com.venomvendor.fullstack;

import android.content.Context;
import android.support.annotation.StringRes;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasErrorText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.junit.Assert.assertEquals;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class ExampleInstrumentedTest {

    @Rule
    public ActivityTestRule<LoginActivity> mActivityRule = new ActivityTestRule<>(
            LoginActivity.class);

    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.venomvendor.fullstack", appContext.getPackageName());
    }

    @Test
    public void withValidInputs() {
        // Type text and then press the button.
        String email = "naa@naa.com";
        String password = "******";

        onView(withId(R.id.email))
                .perform(typeText(email), closeSoftKeyboard());

        onView(withId(R.id.password))
                .perform(typeText(password));

        onView(withId(R.id.email_sign_in_button)).perform(click());

        onView(withId(R.id.email))
                .check(matches(withText(email)));

        // Check that the text was changed.
        onView(withId(R.id.password))
                .check(matches(withText(password)));
    }

    @Test
    public void invalidPasswordError() {
        // Type text and then press the button.
        String inputText = "***";

        onView(withId(R.id.password))
                .perform(typeText(inputText));

        onView(withId(R.id.email_sign_in_button)).perform(click());

        // Check that the text was changed.
        onView(withId(R.id.password))
                .check(matches(withText(inputText)));

        onView(withId(R.id.password))
                .check(matches(hasErrorText(getString(R.string.error_invalid_password))));
    }

    @Test
    public void invalidEmailError() {
        onView(withId(R.id.email))
                .perform(typeText("naa.com"));

        onView(withId(R.id.password))
                .perform(typeText("******"));
        onView(withId(R.id.email_sign_in_button)).perform(click());

        onView(withId(R.id.email))
                .check(matches(hasErrorText(getString(R.string.error_invalid_email))));
    }

    private static String getString(@StringRes int stringResId) {
        return InstrumentationRegistry.getTargetContext().getString(stringResId);
    }
}
